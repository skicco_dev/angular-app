import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeReturnComponent } from './home-return.component';

describe('HomeReturnComponent', () => {
  let component: HomeReturnComponent;
  let fixture: ComponentFixture<HomeReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
