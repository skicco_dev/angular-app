import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
})
export class ChildComponent implements OnInit {
  @Input() inputPost: any;
  @Output() outputPost = new EventEmitter<any>();
  constructor() {}

  ngOnInit(): void {}

  liked(value: any) {
    this.outputPost.emit('you liked ' + value);
  }
  shared(value: any) {
    this.outputPost.emit('you shared ' + value);
  }
}
