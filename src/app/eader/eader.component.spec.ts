import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EaderComponent } from './eader.component';

describe('EaderComponent', () => {
  let component: EaderComponent;
  let fixture: ComponentFixture<EaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
