import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eader',
  templateUrl: './eader.component.html',
  styleUrls: ['./eader.component.css'],
})
export class EaderComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToReturn() {
    alert('goToReturn');
    this.router.navigate(['/']);
  }
}
