import { PostService } from './../services/post.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  textFromComponent: string =
    'Hey this is the text from component displaying via' +
    ' interapolation or one way binding';
  count: number = 0;
  additional: number;
  cities: any[] = [
    'Mumbai',
    'chennai',
    'Kolkata',
    'Bangalore',
    'Hyderabad',
    'Delhi',
  ];
  showName: boolean;
  btnText: string = 'Show';
  users: any[] = [
    { id: 1001, name: 'Peter' },
    { id: 1002, name: 'Eva' },
    { id: 1003, name: 'Adam' },
    { id: 1004, name: 'John' },
    { id: 1005, name: 'Emile' },
    { id: 1006, name: 'Taco' },
    { id: 1007, name: 'Sander' },
  ];
  posts: any[] = [];
  today = new Date();
  constructor(private readonly postService: PostService) {}

  ngOnInit(): void {
    this.postService.getPosts().subscribe((posts) => {
      console.log(posts);
      this.posts = posts;
    });
  }

  incrementCount() {
    this.count = this.count + +this.additional;
  }
  showUsers() {
    this.showName = !this.showName;
    if (this.showName) {
      this.btnText = 'hide';
    } else {
      this.btnText = 'show';
    }
  }

  eventFromChild(value: any) {
    alert(value);
  }
}
