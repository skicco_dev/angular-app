import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css'],
})
export class ReactiveFormsComponent implements OnInit {
  userForm: FormGroup;

  constructor() {
    this.userForm = this.createUserForm();
  }

  ngOnInit(): void {}

  createUserForm() {
    return new FormGroup({
      userNameControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordControl: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    if (this.userForm.valid) {
      alert('form is good');
    } else {
      alert('There are some errors with the comfig');
    }
  }
}
