import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven-forms',
  templateUrl: './template-driven-forms.component.html',
  styleUrls: ['./template-driven-forms.component.css'],
})
export class TemplateDrivenFormsComponent implements OnInit {
  userName: string;
  password: string;
  userErrorMsg: string;
  passwordErrorMsg: string;
  isuserNameEmpty: boolean;
  isPasswordError: boolean;
  constructor() {}

  ngOnInit(): void {}

  submitForm() {
    this.isPasswordError = false;
    this.isuserNameEmpty = false;

    if (
      this.userName === null ||
      this.userName === '' ||
      this.userName === undefined
    ) {
      this.isuserNameEmpty = true;
      this.userErrorMsg = 'user name is required';
    }

    if (
      this.password === null ||
      this.password === '' ||
      this.password === undefined
    ) {
      this.isPasswordError = true;
      this.passwordErrorMsg = 'password is required';
    }
  }
}
